# Proyecto GNUBIES


Hola a tod@s.

Mi proyecto consiste en el desarrollo de un vídeo en el editor de vídeo libre "ShotCut", que va entorno a los símbolos de la Universidad Distrital Francisco José de Caldas.

El objetivo principal de este proyecto además de impulsar en la comunidad universitaria el uso de herramientas y plataformas libres, es el de informar y generar una apropiación de los símbolos de la Universidad Distrital, que muchas veces pasan por alto y que hacen parte de un patrimonio historico y cultural de esta institución.

En este repositorio se va a poder encontrar el proyecto en formato propio de la plataforma ShotCut (extensión .mlt) de forma que se pueda facilitar su edición.

El vídeo se realizó alrededor de la licencia Creative Commons y se encuentra público en la plataforma de Youtube.

Se presenta el enlace:

https://youtu.be/-TOvLoqdNCM



Fuentes de donde se sacaron las imágenes presentes en el vídeo:

Logos de la Universidad Distrital Francisco José de Caldas.

Universidad Distrital Francisco José de Caldas. (18 de Marzo del 2022). Quiénes somos-Símbolos. Universidad Distrital Francisco José de Caldas. Recuperado el 14 de agosto del 2022. https://www.udistrital.edu.co/nuestra-universidad/quienes-somos/simbolos

Fotos de carreras ofrecidas, primeras sedes, primera promoción de graduados, edificio de ingeniería forestal, construcción de la sede macarena.

Universidad Distrital Francisco José de Caldas. (08 de Noviembre del 2021). Quiénes somos-Historia. Universidad Distrital Francisco José de Caldas. Recuperado el 14 de agosto del 2022. https://www.udistrital.edu.co/nuestra-universidad/quienes-somos/historia

Fotos del “Tigre Pérez”

Egresados Universidad Distrital distrital. (27 de Septiembre del 2018). EGRESADOS DESTACADOS-Ramón Pérez EL TIGRE [Archivo de Vídeo ]. Youtube. Recuperado el 14 de Agosto del 2022. https://www.youtube.com/watch?v=_GoP60T0It4

Logo “El tigre Pérez”

wildersteven88. (s.f.). Historia de la Universidad Francisco José de Caldas. Recuperado el 14 de Agosto del 2022. https://www.timetoast.com/timelines/historia-de-la-universidad-francisco-jose-de-caldas-5d50ff8f-1d3a-4c76-b193-543394c1d402

Fotos de Alexandra Olaya Castro.

TED x Talks. (5 de Enero del 2017). El poder de la opción B para romper estereotipos [Archivo de Vídeo]. Youtube. Recuperado el 14 de Agosto del 2022. https://www.youtube.com/watch?v=t2GxMJDsl6Q

Revista Semana. (25 de Junio del 2019). Alexandra Olaya-Castro: la colombiana que está haciendo historia entre los grandes de la física [Archivo de Vídeo]. Youtube. Recuperado el 14 de Agosto del 2022. https://www.youtube.com/watch?v=6D0gwxKyxLs

Jaramillo Arias, M. (2019). “Me dijeron que era una mala pobre por querer salir adelante”: Alexandra Olaya-Castro. Semana. Recuperado el 14 de Agosto del 2022. https://www.semana.com/vida-moderna/articulo/me-dijeron-que-era-una-mala-pobre-por-querer-salir-adelante-alexandra-olaya-castro/620864/

Fotos tomadas adentro de la sede macarena.

Tomadas por Daniel Villescas (editor original del vídeo).






